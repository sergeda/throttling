Throttling service solution

### To run functional tests
sbt test

### To run performance tests for 100 simultaneous users
1. Run service: sbt run
2. Run sbt gatling-it:test in another console window
There will be results in directory target/gatling-it

Number of simultaneous users can be changed in file /src/it/resources/application.conf