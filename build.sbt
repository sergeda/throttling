name := """throttling"""

version := "1.0"

scalaVersion := "2.11.11"


// Change this to another test framework if you prefer
libraryDependencies ++= Seq (
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.typesafe" % "config" % "1.3.1",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test,
  "com.typesafe.akka" %% "akka-http" % "10.0.6",
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.2.5" % "test,it",
  "io.gatling" % "gatling-test-framework" % "2.2.5" % "test,it"
)

//concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)

enablePlugins(GatlingPlugin)
