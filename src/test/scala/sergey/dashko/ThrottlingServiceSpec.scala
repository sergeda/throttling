package sergey.dashko


import org.scalatest.Matchers._
import akka.actor.{ActorSystem, Scheduler}
import org.scalatest._
import org.scalamock.scalatest.MockFactory
import org.scalatest.OneInstancePerTest

import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}
import scala.concurrent.{Await, Future}


/**
  * Created by sergey on 5/6/17.
  */
class ThrottlingServiceSpec extends FlatSpec with MockFactory with PrivateMethodTester with OneInstancePerTest {
  val slaServStub: SlaService = stub[SlaService]
  implicit val system = ActorSystem("my-system")
  implicit val executionContext = system.dispatcher
  val scheduler: Scheduler = system.scheduler
  val delayedIncorrectToken: Future[Sla] = akka.pattern.after(FiniteDuration(250, MILLISECONDS), using = scheduler)(Future.failed(
    new IllegalArgumentException("Unauthorized")))
  val delayedAnton: Future[Sla] = akka.pattern.after(FiniteDuration(250, MILLISECONDS), using = scheduler)(Future.successful(Sla("anton", 5)))
  val delayedJack: Future[Sla] = akka.pattern.after(FiniteDuration(250, MILLISECONDS), using = scheduler)(Future.successful(Sla("jack", 15)))
  val delayedIvan: Future[Sla] = akka.pattern.after(FiniteDuration(250, MILLISECONDS), using = scheduler)(Future.successful(Sla("ivan", 25)))
  slaServStub.getSlaByToken _ when "q" returns delayedAnton
  slaServStub.getSlaByToken _ when "w" returns delayedJack
  slaServStub.getSlaByToken _ when "e" returns delayedIvan
  slaServStub.getSlaByToken _ when "t" returns delayedIvan
  slaServStub.getSlaByToken _ when "r" returns delayedIncorrectToken

  val throttlingService = new ThrottlingServiceImplem(slaServStub)
  val rpsIntervalDiff = PrivateMethod[(Long, Long, Double)]('rpsIntervalDiff)
  val userRps = PrivateMethod[TrieMap[String, (Long, Long, Double)]]('_userRps)
  val slaRequests = PrivateMethod[TrieMap[String, (Future[Sla], Long)]]('_slaRequests)

    "Calling rpsIntervalDiff without providing current Rps" should "return new Rps" in {
      val currentTime = System.currentTimeMillis()
      assert((throttlingService invokePrivate rpsIntervalDiff(Sla("anton", 15), None, currentTime)) === (currentTime, currentTime, 15 / 10D))
    }

    "Calling rpsIntervalDiff providing expired Rps" should "return new Rps" in {
      val currentTime = System.currentTimeMillis()
      val rps = (currentTime - 1001, currentTime - 500, 3d)
      assert((throttlingService invokePrivate rpsIntervalDiff(Sla("anton", 15), Some(rps), currentTime)) === (currentTime, currentTime, 15 / 10D))
    }

    "Calling rpsIntervalDiff providing Rps with modification time less then count interval" should "return unchanged Rps" in {
      val currentTime = System.currentTimeMillis()
      val rps = (currentTime - 500, currentTime - (throttlingService.interval - 1), 3d)
      assert((throttlingService invokePrivate rpsIntervalDiff(Sla("anton", 15), Some(rps), currentTime)) === rps)
    }

    "Calling rpsIntervalDiff providing Rps with time bigger then count interval" should "return correctly changed Rps" in {
      val currentTime = System.currentTimeMillis()
      val rps = (currentTime - 500, currentTime - (throttlingService.interval * 3), 0d)
      assert((throttlingService invokePrivate rpsIntervalDiff(Sla("anton", 15), Some(rps), currentTime)) === rps.copy(_3 = 4.5, _2 = currentTime))
    }

    "Calling rpsIntervalDiff in cycle during 1000 ms" should "return correct Rps for given Sla" in {
      val startTime = System.currentTimeMillis()
      val stopTime = startTime + 1000
      val steps = for (t <- startTime to stopTime) yield t
      var results = List[(Long, Long, Double)]()
      while(stopTime - System.currentTimeMillis() > 0){
        if(steps.contains(System.currentTimeMillis())){
            results = (throttlingService invokePrivate rpsIntervalDiff(Sla("anton", 15), results.headOption, System.currentTimeMillis())) :: results
        }
      }
      assert(results.head._3 === 15d)
    }


    "Calling isRequestAllowed without providing any token" should "give user correct GraceRps and allow request in less then 5ms" in {
      val start = System.currentTimeMillis()
      throttlingService.isRequestAllowed(None)
      val end = System.currentTimeMillis()
      assert(end - start < 5)
      (throttlingService invokePrivate userRps()).get("unauthorized") should not be empty
      (throttlingService invokePrivate userRps())("unauthorized")._3  shouldEqual 3d
    }

  "Calling isRequestAllowed without providing any token" should "not call SlaService" in {
    throttlingService.isRequestAllowed(None)
    (throttlingService invokePrivate slaRequests()) shouldBe empty
  }

  "Calling isRequestAllowed providing token" should "call SlaService but until request success give user correct GraceRps in less then 5ms" in {
    val start = System.currentTimeMillis()
    throttlingService.isRequestAllowed(Some("w"))
    val end = System.currentTimeMillis()
    assert(end - start < 5)
    (throttlingService invokePrivate slaRequests()).get("w") should not be empty
    (throttlingService invokePrivate userRps()).get("unauthorized") should not be empty
    (throttlingService invokePrivate userRps()) ("unauthorized")._3 shouldEqual 3d
  }


  "Calling isRequestAllowed providing token" should "give user correct GraceRps for the first 100 ms" in {
    val startTime = System.currentTimeMillis()
    val stopTime = startTime + 100
    val steps = for (t <- startTime to stopTime) yield t
    var results = List[Boolean]()
    while (stopTime - System.currentTimeMillis() > 0) {
      if (steps.contains(System.currentTimeMillis())) {
        results = throttlingService.isRequestAllowed(Some("w")) :: results
      }
    }

    assert(results.count(_ == true) === throttlingService.graceRps / 10)
  }

  "Calling isRequestAllowed providing token" should "give user correct Rps after getting result from SlaService" in {
    var results = List[Boolean]()
    //making initial request, should allow request from GraceRps pool
    assert(throttlingService.isRequestAllowed(Some("w")) === true)
    (throttlingService invokePrivate slaRequests()).get("w") should not be empty
    (throttlingService invokePrivate userRps()).get("unauthorized") should not be empty

    //getting future of request to SlaService
    val (slaReq, _) = (throttlingService invokePrivate slaRequests()) ("w")
    Await.result(slaReq, FiniteDuration(300, MILLISECONDS))
    val startTime = System.currentTimeMillis()
    val stopTime = System.currentTimeMillis() + 1000
    val steps = for (t <- startTime to stopTime) yield t
    while (stopTime - System.currentTimeMillis() > 0) {
      if (steps.contains(System.currentTimeMillis())) {
        results = throttlingService.isRequestAllowed(Some("w")) :: results
      }
    }
    assert(results.count(_ == true) === 15)

  }


}
