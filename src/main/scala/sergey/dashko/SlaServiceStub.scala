package sergey.dashko


import scala.concurrent.Future

/**
  * Created by sergey on 5/14/17.
  */
class SlaServiceStub extends SlaService{
  def getSlaByToken(token: String): Future[Sla] = Future.successful(Sla("ivan", 10000))
}
