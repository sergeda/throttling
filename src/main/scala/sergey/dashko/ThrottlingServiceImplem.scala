package sergey.dashko


import scala.collection.concurrent.TrieMap
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Created by sergey on 5/6/17.
  */

class ThrottlingServiceImplem(val slaService: SlaService) extends ThrottlingService {
  type Token = String
  type User = String
  type PeriodStart = Long
  type LastModified = Long
  type RequestsAvailable = Double
  type Rps = (PeriodStart, LastModified, RequestsAvailable)
  private[this] val c: Config = ConfigFactory.load()
  private[this] val slaRequests = TrieMap[Token, (Future[Sla], LastModified)]()
  private[this] val userRps = TrieMap[User, Rps]()

  private[this] def _userRps = userRps //needed for Scalatest to access userRps
  private[this] def _slaRequests = slaRequests //needed for Scalatest to access slaRequests
  val graceRps: Int = c.getInt("throttling.graceRps")
  val interval = 100

  def isRequestAllowed(token: Option[Token]): Boolean = {

    val sla: Sla = token.flatMap { t =>
      val customSla = slaRequests.get(t)
      if (customSla.isEmpty) {
        addSlaRequest(t)
        None
      } else {
        customSla.flatMap { case (sf, _) =>
          sf.value match {
            case Some(Success(v)) => Some(v)
            case Some(Failure(_)) =>
              addSlaRequest(t)
              None
            case _ => None
          }
        }
      }
    }.getOrElse(Sla("unauthorized", graceRps))

    if (sla.rps > 0) {
      val mUserRps: Option[Rps] = userRps.get(sla.user)
      val updatedRps = rpsIntervalDiff(sla, mUserRps, System.currentTimeMillis())
      addRps(sla.user, mUserRps, updatedRps)
      isRequestAllowed(updatedRps)
    } else {
      false
    }
  }


  private def addSlaRequest(token: Token): Unit = {
    val f = slaService.getSlaByToken(token)
    val currentTime = System.currentTimeMillis()
    slaRequests += (token -> (f, currentTime))
  }

  private def addRps(user: User, currentRps: Option[Rps], newRps: Rps): Unit = {
    if (isRequestAllowed(newRps)) {
      userRps += (user -> newRps.copy(_3 = newRps._3 - 1))
    } else {
      if (currentRps.isEmpty) userRps += (user -> newRps)
    }
  }


  private def rpsIntervalDiff(sla: Sla, currentRps: Option[Rps], currentTime: Long): Rps = {
    val newRps = (currentTime, currentTime, sla.rps / 10D)
    currentRps.map {
      case (startTime, _, _) if (currentTime - startTime) > 1000 =>
        newRps
      case rps@(_, lastModified, _) if (currentTime - lastModified) <= interval =>
        rps
      case (startTime, lastModified, leftRequests) =>
        val intervalsPassed: Long = (currentTime - lastModified) / interval
        (startTime, currentTime, sla.rps / 10D * intervalsPassed + leftRequests)
    }.getOrElse {
      newRps
    }

  }

  private def isRequestAllowed(userRps: Rps): Boolean = {
    if (userRps._3 >= 1) true else false
  }

  /**
    * Method can be used to remove old requests
    */
  private def cleanOldRequests(maxLife: Long): Unit = {
    slaRequests.dropWhile { case (_, (_, time)) => time > maxLife }
  }

  /**
    * Method can be used to remove old RPS
    */
  private def cleanOldRps(maxLife: Long): Unit = {
    userRps.dropWhile { case (_, (_, time, _)) => time > maxLife }
  }

}
