package sergey.dashko

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.TooManyRangesRejection

import scala.io.StdIn

/**
  * Created by sergey on 5/12/17.
  */
object WebServer{
  def main(args: Array[String]) {
    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher
    val slaServStub = new SlaServiceStub
    val throttlingService = new ThrottlingServiceImplem(slaServStub)
    val route =
        parameters('token.?) { mToken =>
          mToken.map{ token =>
            if(throttlingService.isRequestAllowed(Some(token))){
              complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "OK"))
            } else {
              reject(TooManyRangesRejection(100))
            }
          }.getOrElse{
            complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "OK"))
          }
        }



    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }


}
