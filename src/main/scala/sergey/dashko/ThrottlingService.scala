package sergey.dashko


/**
  * Created by sergey on 5/6/17.
  */
trait ThrottlingService {
  val graceRps:Int
  val slaService: SlaService
  // Should return true if the request is within allowed RPS.
  def isRequestAllowed(token:Option[String]): Boolean
}
