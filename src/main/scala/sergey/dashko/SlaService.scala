package sergey.dashko

import scala.concurrent.Future

/**
  * Created by sergey on 5/6/17.
  */
case class Sla(user:String, rps:Int)
trait SlaService {
  def getSlaByToken(token:String):Future[Sla]
}
