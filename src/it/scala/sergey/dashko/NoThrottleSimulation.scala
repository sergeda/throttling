package sergey.dashko

import io.gatling.http.Predef._
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import scala.concurrent.duration._

/**
  * Created by sergey on 5/14/17.
  */
class NoThrottleSimulation extends BasicSimulation{
  lazy val scenarios = List(
    NoThrottleSimulation.run()
  )
}

object NoThrottleSimulation {
  def run(): ChainBuilder =
    during(5 seconds) {
      exec(http("NoThrottleSimulation")
        .get("/")
        .check(status.is(200))
      )
    }
}