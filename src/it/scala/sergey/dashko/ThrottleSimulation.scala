package sergey.dashko

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration._

/**
  * Created by sergey on 5/14/17.
  */
class ThrottleSimulation extends BasicSimulation{
  lazy val scenarios = List(
    ThrottleSimulation.run()
  )
}

object ThrottleSimulation {
  def run(): ChainBuilder =
    during(5 seconds) {
      exec(http("ThrottleSimulation")
        .get("/?token=token")
        .check(status.is(200))
      )
    }
}