package sergey

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by sergey on 5/14/17.
  */
package object dashko {
  val c: Config = ConfigFactory.load()
  val users: Int = c.getInt("gatling.run.users")
  val baseUrl: String = c.getString("gatling.run.baseUrl")
}
