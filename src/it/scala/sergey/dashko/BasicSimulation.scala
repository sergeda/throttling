package sergey.dashko

import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder


/**
  * Created by sergey on 5/14/17.
  */
abstract class BasicSimulation extends Simulation{
  val scenarios: List[ChainBuilder]

  val httpConf: HttpProtocolBuilder = http
    .baseURL(baseUrl)
    .acceptHeader("text/html")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val scn: ScenarioBuilder = scenario(this.getClass.getSimpleName).exec(
    scenarios
  )

  setUp(
    scn.inject(atOnceUsers(users))
  ).protocols(httpConf)
}
